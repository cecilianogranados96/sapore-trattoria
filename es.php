<?php
$lang['home'] = "Inicio";
$lang['about'] = "Nosotros";
$lang['menu'] = "Menu";
$lang['galery'] = "Galeria";
$lang['reservation'] = "Reservaciones";
$lang['contact'] = "Contacto";
$lang['slider1'] = "Bienvenido a la cocina italiana";
$lang['slider2'] = "La mejor comida italiana EN COSTA RICA";
$lang['sub'] = "Todas nuestras pastas tienen pasión.";
$lang['about_us'] = "Sobre Nosotros";
$lang['text_about'] = "Cada plato que preparamos y servimos lleva lo mejor de cada una de las personas que laboramos en el restaurante, cada plato es pasión hecha pasta artesanal.";
$lang['the_history'] = "La Historia";
$lang['text_history'] =  "Todos nuestros platos son hechos según las recetas originales 
de la verdadera comida Italiana, en nuestra propia cocina y sobre 
todo con una atención especial para todos aquellos que se inclinan
por platos sabrosos y balanceados entre aporte nutritivo y calórico.
Los ingredientes usados con lo mejor que ofrece nuestra tierra, con
aceites de oliva extravirgen certificado, tomates de Salerno, pastas
caseras o de grano duro de alta calidad, embutidos, quesos italianos,
entre otros... Buon appetito!";
$lang['text_history2'] =  "";//"Sabemos cuál es la reacción de cualquier individuo cuando, en cualquier caótica ciudad industrial del mundo, cansado y deprimido, encuentra un letrero de comida italiana: siente que su corazón se conforta.";
$lang['text_history3'] = "La cocina es, sin lugar a dudas, una parte muy importe de la cultura italiana.Conocida en todo el mundo, amada y continuamente imitada, ha sido capaz de dar placer y alegría de vivir en todas las latitudes. Sabemos cuál es la reacción de cualquier individuo cuando, en cualquier caótica ciudad industrial del mundo, cansado y deprimido, encuentra un letrero de comida italiana: siente que su corazón se conforta.Se trata de una cocina rica, nutritiva y saludable, transmitida por siglos a través de la vida familiar. Su carácter es esencialmente campesino y, como tal, está vinculada a nuestra tierra y a los frutos que produce en el curso de las estaciones: en consecuencia, es una cocina genuina y basada en ingredientes naturales.";
$lang['text_history4'] = "";//"Su carácter es esencialmente campesino y, como tal, está vinculada a nuestra tierra y a los frutos que produce en el curso de las estaciones: en consecuencia, es una cocina genuina y basada en ingredientes naturales.";
$lang['hours'] =  "Horario";
$lang['call'] = "Llamar para hacer reservas";
$lang['hours1'] = "Lunes a Jueves";
$lang['hours2'] = "Sábado";
$lang['hours3'] =  "Sunday";
$lang['Reservations'] = "Reservaciones: 2222 8901/2222 8906";
$lang['menu1'] = "Pasta ancha hecha artesanalmente, servida con
carne de ternera preparada con una receta
clasica estilo romano";
$lang['menu2'] = "Trozos de atun fresco, salteado con tomate y
cebolla roja y combinados perfectamente con
fettuccinis de albahaca servido sobre arúgula";
$lang['menu3'] = "Pollo parrillado en trozos, hongos silvestres,
tomate seco en una salsa de parmesano";
$lang['menu4'] = "Tradicional arroz italiano, camarones, ajo, aceite
de oliva, vino blanco, jugo de tomate y pereji";
$lang['menu5'] = "Embutidos mixtos italianos con berenjena y
zuquini a la parrilla";
$lang['menu6'] = "Pecorino, parmiggano, scamorza,
mozzarella fresca con mermelada de higos
y manzana verde";
$lang['menu7'] = "Champiñones del bosque rellenos de
espinaca, tocineta, trufas y mozzarella
gratinados con queso parmesano y
servidos en una salsa de tomate fresco y
albahaca";
$lang['menu8'] = "Lonjas de atún fresco del pacifico
marinado con sal y pimienta, servido con
arúgula, alcaparras, tomate cereza y
aderezo citrico";

$lang['menu_complete'] = "Ver menú completo";
$lang['galery_text'] ="Nuestra pasión es la cocina italiana.";
$lang['reservation_text'] = "¡Haga su reserva en linea! dentro de unos cuantos minutos le estaremos contestando.";
$lang['welcome'] =  "Bienvenido a Sapore Trattoria";
$lang['make_reservation'] = "Hacer una reservacion";
$lang['date'] = "Fecha";
$lang['time'] = "Tiempo";
$lang['party'] = "Party";
$lang['contact_details'] = "Contact Details";
$lang['email'] = "Email";
$lang['name'] = "Nombre";
$lang['phone'] = "Teléfono";
$lang['menssage'] = "Mensaje";
$lang['make'] =  "Hacer Reservacion";
$lang['direction'] = "San Jose, Avenida Segunda, 1 cuadra al oeste de la Plaza la Democracia..";
$lang['subject'] = "Asunto";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";




?>