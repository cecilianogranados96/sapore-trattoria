<?php 
	if(isset($_GET['lang'])){
		if ($_GET['lang'] == 1){
			include "es.php";
		}
		else{
			include "en.php";
		}
	}else{
        include "es.php";
    }
	
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sapore Trattoria</title>
    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Yellowtail%7cCabin:400,500,600,700,400italic,700italic%7cLibre+Baskerville:400italic%7cGreat+Vibes%7cOswald:400,300,700%7cOpen+Sans:600italic,700' rel='stylesheet' type='text/css'>

    <!-- META TAGS -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />

    <!-- CSS STYLESHEETS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
 	<link rel="stylesheet" href="css/elixir.css" />
    <link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="js/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="css/YTPlayer.css" rel="stylesheet">
 	<link rel="stylesheet" href="css/swipebox.css">


 <!-- Colors Style -->
    <link rel="stylesheet" href="css/color/wine.css">
    <link rel="stylesheet" href="css/color/bluegray.css">
    <link rel="stylesheet" href="css/color/orange.css">
    <link rel="stylesheet" href="css/color/black.css">
    <link rel="stylesheet" href="css/color/yellow.css">
    <link rel="stylesheet" href="css/color/green.css">
    <link rel="stylesheet" href="css/color/gold.css">
 
 
    <!--[if lt IE 9]>
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <script src="./js/html5shiv.js"></script>
        <script src="./js/respond.js"></script>
    <![endif]-->
</head>
<body>
	<div id="mask">
            <div class="loader">
              <img src="svg-loaders/tail-spin.svg" alt='loading'>
            </div>
        </div>

        <!-- BEGIN HEADER -->
        <header id="header" role="banner">
            <div class="jt_row container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                        </button>
                    <a class="navbar-brand normal logo" href="#home"><img src="images/Logo2.png"><!-- <span>american food</span> --></a>
                    <a class="navbar-brand mini" href="#home"><img src="images/Logo2.png"><!-- <span class="dark">american food</span> --></a>
                    <a class="navbar-brand mini darker" href="#home"><img src="images/Logo2.png"><!-- <span>american food</span> --></a>
                </div>
                <!-- BEGIN NAVIGATION MENU-->
                <nav class="collapse navbar-collapse navbar-right navbar-main-collapse" role="navigation">
                    <ul id="nav" class="nav navbar-nav navigation">
                        <li class="page-scroll menu-item"><a href="index.php"><?php echo $lang['home']; ?></a></li>
                        <li class="page-scroll menu-item"><a href="#about"><?php echo $lang['about']; ?></a></li>
                        <li class="page-scroll menu-item"><a href="menu.php"><?php echo $lang['menu']; ?></a></li>
                        <li class="page-scroll menu-item"><a href="#gallery"><?php echo $lang['galery']; ?></a></li>
                        <li class="page-scroll menu-item"><a href="#reservations"><?php echo $lang['reservation']; ?></a></li>
                        <li class="page-scroll menu-item"><a href="#contact"><?php echo $lang['contact']; ?></a></li>
                         <li class="page-scroll menu-item">
                             <a href="?lang=1"> <img src="images/espanol.png" width="35"> </a>
                             <a href="?lang=2"> <img src="images/ingles.png" width="35"> </a> 
                        </li>
                         
                    </ul>
                </nav>
                <!-- EN NAVIGATION MENU -->
                </div>
        </header>
        <!-- END HEADER -->
        <!-- BEGIN HOME SLIDER SECTION -->
        <section id="home-slider">
            <div class="overlay"></div>
            <!-- SLIDER IMAGES -->
            <div id="owl-main" class="owl-carousel">
                <div class="item"><img src="images/slider/1.JPG" alt="Slide 01"></div>
                <div class="item"><img src="images/slider/2.JPG" alt="Slide 02"></div>
                <div class="item"><img src="images/slider/3.JPG" alt="Slide 03"></div>
                <div class="item"><img src="images/slider/4.JPG" alt="Slide 04"></div>
                <div class="item"><img src="images/slider/5.JPG" alt="Slide 05"></div>
            </div>
            <!-- SLIDER CONTENT -->
            <div class="slide-content">
                <div class="voffset100"></div>
                <center><img src="images/logo.png" width="300px"></center>
                <div id="owl-main-text" class="owl-carousel">
                    <div class="item">
                        <h2><?php echo $lang['slider1']; ?></h2>
                    </div>
                    <div class="item">
                        <h2><?php echo $lang['slider2']; ?></h2>
                    </div>
                </div>
                <div class="slide-sep"></div>
                <p><?php echo $lang['sub']; ?></p>
                
            </div>
            <!-- BOTTOM ANIMATED ARROW -->
            <a href="#about" class="page-scroll menu-item"><div class="mouse"><span></span></div></a>
        </section>
        <!-- END HOME SLIDER SECTION -->





        <!-- BEGIN ABOUT SECTION -->
        <section id="about" class="section about">
            <div class="container">
                <div class="jt_row jt_row-fluid row">
                    <div class="col-md-12 jt_col column_container">
                        <h2 class="section-title"><?php echo $lang['about_us']; ?></h2>
                    </div> 
                    <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                        <div class="section-subtitle">
                         <?php echo $lang['text_about']; ?> 
                        </div>
                    </div>


                    <div class="col-md-6 jt_col column_container">
                        <h2 class="heading font-smoothing"> <?php echo $lang['the_history']; ?></h2>
                        <p class="text">
                            <?php echo $lang['text_history']; ?>
                        </p>
                        <p class="text"><?php echo $lang['text_history2']; ?>
                        </p>
                        
                        <div class="ornament"></div>
                    </div>  

                    <div class="col-md-6 jt_col column_container">
                    <div class="voffset40"></div>
                        <div id="owl-about" class="owl-carousel">
                                <div class="item"><img src="images/about.jpg" alt=""></div>
                        </div>
                    </div> 
                </div>
                <div class="jt_row jt_row-fluid row">
                    <div class="col-md-6 jt_col column_container">
                        <div class="voffset10"></div>
                        <div id="owl-about2" class="owl-carousel">
                                   <div class="item"><img src="images/2.jpg" alt=""></div>
                                <div class="item"><img src="images/3.jpg" alt=""></div>
                                <div class="item"><img src="images/4.jpg" alt=""></div>
                        </div>
                    </div>  



                    <div class="col-md-6 jt_col column_container">

                        <p class="text">
                            <?php echo $lang['text_history3']; ?>
                           
                        </p>
                        <p class="text">
                            <?php echo $lang['text_history4']; ?>
                           
                        </p>
                    </div> 
                </div>

            </div>
            
        </section>
        <!-- END ABOUT SECTION -->




        <!-- BEGIN TIMETABLE SECTION -->
        <section id="timetable" class="section timetable parallax">
            <div class="container">
                    <div class="jt_row jt_row-fluid row">
                        <div class="col-md-12 jt_col column_container">
                            <h2 class="section-title"><span class="timetable-decorator"></span><span class="opening-hours">  <?php echo $lang['hours']; ?></span><span class="timetable-decorator2"></span></h2>
                        </div> 
                        <div class="col-md-12 jt_col column_container">    
                            <div class="section-subtitle">
                             <?php echo $lang['call']; ?>
                            </div>
                        </div>
                        <div class="col-md-2  jt_col column_container">   </div>
                        <div class="col-md-3  jt_col column_container">                        
                            <div class="section-subtitle days">
                               <?php echo $lang['hours1']; ?>
                            </div>
                            <div class="section-subtitle hours">
                                11:30 am <br>to<br> 10:30 pm
                            </div>
                        </div>
                        <div class="col-md-3  jt_col column_container">    
                            <div class="section-subtitle days">
                              <?php echo $lang['hours2']; ?> <br>
                            </div>
                            <div class="section-subtitle hours">
                               12:00 pm  <br>to<br>  11:30 pm
                            </div>
                        </div>
                        
                        <div class="col-md-3 jt_col column_container">    
                            <div class="section-subtitle days">
                              <?php echo $lang['hours3']; ?><br>
                            </div>
                            <div class="section-subtitle hours">
                              12:00 pm  <br>to<br> 7:00 pm
                            </div>
                        </div>
                       
                        <div class="col-md-12 jt_col column_container">    
                            <div class="section-subtitle hours">
                              <?php echo $lang['Reservations']; ?>
                           
                            </div>
                        </div>

                    </div>
            </div>
        </section>
        <!-- END TIMETABLE SECTION -->




        <!-- BEGIN MENU SECTION -->
        <section id="menu" class="section menu">

            <div class="container">
                    <div class="jt_row jt_row-fluid row">
                        <div class="col-md-12 jt_col column_container">
                         <div class="voffset100"></div>
                            <img class="center menu-logo" src="images/logo.png"  width="200px" alt="Menu Logo">
                           
                        </div>
                        
                        <div class="col-md-6 jt_col column_container">    
                            <h3>La nostra Pasta:</h3>
                            <ul>
                                <li>
                                    Pappardelle con Ossobuco
                                    <div class="detail">  <?php echo $lang['menu1']; ?><span class="price">¢6.450</span></div>
                                </li>
                                <li>
           Scialatielli al Tonno
                                    <div class="detail">  <?php echo $lang['menu2']; ?><span class="price">¢9.450</span></div>
                                </li>
                                <li>
                                    Fettuccine Pollo & Funghi
                                    <div class="detail"><?php echo $lang['menu3']; ?>
    <span class="price">¢8.450</span></div>
                                </li>
                                <li>
Risotto al Gamberi
                                    <div class="detail"><?php echo $lang['menu4']; ?><span class="price">¢9.950</span></div>
                                </li>
                      
                            </ul>
                        </div>
                      
                        <div class="col-md-6 jt_col column_container">    
                            <h3>I nostri Antipasti:</h3>
                            <ul>
                                <li>Antipasto Sapore
                                    <div class="detail"><?php echo $lang['menu5']; ?><span class="price">¢6.950</span></div>
                                </li>
                                <li>Tavola di formaggi
                                    <div class="detail"><?php echo $lang['menu6']; ?><span class="price">¢6.450</span></div>
                                </li>
                                <li>Funghi Ripieni
                                    <div class="detail"><?php echo $lang['menu7']; ?><span class="price">¢4.850</span></div>
                                </li>
                                <li>Carpaccio di Tonno
                                    <div class="detail"><?php echo $lang['menu8']; ?><span class="price">¢5.950</span></div>
                                </li>
                
                            </ul>
                        </div>
                      
                        <div class="col-md-4 col-md-offset-4 jt_col column_container">    
                            <a target="_blank" class="button menu center"><?php echo $lang['menu_complete']; ?></a>
                        </div>
                        
                    </div>
            </div>

        </section>
        <!-- END MENU SECTION -->

        <!-- BEGIN GALLERY SECTION -->
        <section id="gallery" class="section gallery dark">
                <div class="jt_row jt_row-fluid row">
                    <div class="col-md-12 jt_col column_container">
                            <h2 class="section-title"><?php echo $lang['galery']; ?></h2>
                            <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                                <div class="section-subtitle">
                                   <?php echo $lang['galery_text']; ?> 
                                </div>
                    </div>
                            
                    </div> 

                    <div class="col-md-12 jt_col column_container">
                        <div class="portfolio">
         
                     
         
                     
                            <article class="entry complements">
                                <a class="swipebox" href="images/gallery/4.JPG">
                                <img src="images/gallery/4.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article>
                     
                            <article class="entry complements">
                                <a class="swipebox" href="images/gallery/5.JPG">
                                <img src="images/gallery/5.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article>
                     
                            <article class="entry dessert">
                                <a class="swipebox" href="images/gallery/6.JPG">
                                <img src="images/gallery/6.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article>
                     
                            <article class="entry burgers">
                                <a class="swipebox" href="images/gallery/7.JPG">
                                <img src="images/gallery/7.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article>
                     
                            <article class="entry dessert">
                                <a class="swipebox" href="images/gallery/8.JPG">
                                <img src="images/gallery/8.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article> 
                                
                            <article class="entry burgers">
                                <a class="swipebox" href="images/gallery/9.JPG">
                                <img src="images/gallery/9.JPG" class="img-responsive" alt=""/>
                                <span class="magnifier"></span>
                                </a>
                            </article>
                     
                     
                        </div>
                    </div> <!-- End .jt_col -->
                </div> <!-- End .jt_row -->
                <div class="voffset200"></div>
        </section>
        <!-- END GALLERY SECTION -->




        <!-- BEGIN RESERVATIONS SECTION -->
        <section id="reservations" class="section reservations">
            <div class="container">
                    <div class="jt_row jt_row-fluid row">
                        <div class="col-md-12 jt_col column_container">
                            <h2 class="section-title"><?php echo $lang['reservation']; ?>  </h2>
                        </div> 
                        <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                            <div class="section-subtitle">
                                <?php echo $lang['reservation_text']; ?>  
                            </div>
                        </div>
                        <div class="col-md-12 jt_col column_container">
                            <center>
                        <img src="images/logo.png" width="200px">
                            </center>
                        <div class="voffset40"></div>

                            <h4><span class="above">   <?php echo $lang['welcome']; ?> </span></h4>
                            <h3> <?php echo $lang['make_reservation']; ?></h3>
                            <h4><span> <?php echo $lang['hours']; ?></span></h4>
                            <div class="voffset50"></div>
                            <p><strong> <?php echo $lang['hours1']; ?>:</strong> 11:30 am to 10:30 pm<strong>  <?php echo $lang['hours2']; ?>:</strong> 12:00 pm to 11:30 pm<strong>  <?php echo $lang['hours3']; ?>: </strong>12:00 pm to 7:00 pm</p>
                            
                
                            <h3 class="reservation-phone">+506 2222 8901</h3>
                        </div> 

                        <form action="reservaciones.php" method="post" class="contact-form">
                            <div class="col-md-5 col-md-offset-1 jt_col column_container">    
                                
                                <input type="text" id="date" name="date" placeholder="  <?php echo $lang['date']; ?>  " class="text date required" >
                                <input type="text" id="time" name="time" placeholder="  <?php echo $lang['time']; ?>  " class="text time required" >
                                <input type="text" id="party" name="party" placeholder="  <?php echo $lang['party']; ?>  " class="text party required" >
                            </div>

							
                            <div class="col-md-5 jt_col column_container">    
                                
                                <input type="text" id="reservation_name" name="reservation_name" placeholder="<?php echo $lang['name']; ?>" class="text reservation_name required" >
                                <input type="email" id="reservation_email" name="reservation_" class="tex email required" placeholder="<?php echo $lang['email']; ?>" >
                                <input type="text" id="reservation_phone" name="reservation_phone" placeholder="<?php echo $lang['phone']; ?>" class="text reservation_phone required">
                            </div>

                            <div class="col-md-10 col-md-offset-1 jt_col column_container">    
                                <textarea id="reservation_message" name="reservation_message" class="text area required" placeholder="<?php echo $lang['menssage']; ?>" rows="6"></textarea>
                            </div>
                            <div class="col-md-4 col-md-offset-4 jt_col column_container">  
                           
                                <input type="submit" class="button center" value="<?php echo $lang['make']; ?>" >
                            </div>


                            
                        </form>
                        <div class="col-md-12 jt_col column_container">
                        <div class="voffset60"></div>
                            <div class="ornament"></div>
                        </div>
                    </div>
            </div>
        </section>
        <!-- END RESERVATIONS SECTION-->






        <!-- BEGIN CONTACT SECTION -->
        <section id="contact" class="section contact dark">
            <div class="container">
                <div class="jt_row jt_row-fluid row">

                    <div class="col-md-12 jt_col column_container">
                        <h2 class="section-title"><?php echo $lang['contact']; ?></h2>
                    </div>
                    <div class="col-md-6 col-md-offset-3 jt_col column_container">
                        <div class="section-subtitle">
                           <?php echo $lang['direction']; ?> 
                        </div>
                    </div>
                        <form action="contacto.php" method="post" class="contact-form">
                                <div class="col-md-6 jt_col column_container">    
                                    <input type="text" id="name" name="name" class="text name required" placeholder="<?php echo $lang['name']; ?> " >
                                    <input type="email" id="email" name="email" class="tex email required" placeholder="<?php echo $lang['email']; ?> " >
                                    <input type="text" id="subject" name="subject" placeholder="<?php echo $lang['subject']; ?> " >
                                </div>

                                <div class="col-md-6 jt_col column_container">    
                                     <textarea id="message" name="message" class="text area required" placeholder="<?php echo $lang['menssage']; ?>" rows="10"></textarea>
                                </div>

                                <div class="col-md-4 col-md-offset-4 jt_col column_container">
                          
                                <input type="submit" class="button contact center" value="Submit" >
                                </div>
                                
                            </form>
                    <div class="voffset100"></div>
                </div>
                    <div class="voffset50"></div>
            </div>
            
        </section>
        <!-- END CONTACT SECTION -->
        <!-- BEGIN MAP SECTION -->
        <section id="maps">
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            <div class="map-content">
                <div class="wpgmappity_container inner-map" id="wpgmappitymap"></div>
            </div>
        </section>
        <!-- END MAP SECTION -->

        <!-- BEGIN FOOTER -->
        <footer id="footer" class="section" role="contentinfo">
            <div class="container">
                <ul class="social">
                    <li><a href="https://www.facebook.com/SaporeTrattoria/"  target="_blank" class="icon tw"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/SaporeTrattoria/" target="_blank" class="icon fb"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="icon in"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#" class="icon go"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </footer>
    <center>
    
<div id="TA_certificateOfExcellence124" class="TA_certificateOfExcellence">
<ul id="0I2Zq5" class="TA_links Ig3tvsMSxpYM">
<li id="1j7Hz2" class="H6UFXvanA">
<a target="_blank" href="https://www.tripadvisor.com/Restaurant_Review-g309293-d3845939-Reviews-Sapore_Trattoria-San_Jose_San_Jose_Metro_Province_of_San_Jose.html"><img src="https://www.tripadvisor.com/img/cdsi/img2/awards/CoE2015_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a>
</li>
</ul>
</div>
<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=124&amp;locationId=3845939&amp;lang=en_US&amp;year=2015&amp;display_version=2"></script>
    </center>
    
    
        <!-- END FOOTER -->
    <a href="#0" class="cd-top">Top</a>
    <!-- BEGIN Scripts-->

    <script src="js/modernizr.custom.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/pathLoader.js"></script>
    <script src="js/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/jquery.inview.js"></script>
    <script src="js/jquery.nav.js"></script>
    <script src="js/jquery.mb.YTPlayer.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/default.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.swipebox.js"></script>

<script type="text/javascript">
;( function( $ ) {

    $( '.swipebox' ).swipebox();

} )( jQuery );
</script>


<!-- END Scripts -->
</body>

<!-- Mirrored from jellythemes.com/themes/elixir/02/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Feb 2016 01:15:52 GMT -->
</html>
